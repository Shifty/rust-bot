#[macro_use] extern crate serde_derive;
#[macro_use] extern crate log;
extern crate env_logger;

extern crate serenity;
use serenity::{
    client::Client,
    framework::standard::StandardFramework,
    model::id::UserId
};

use std::collections::{HashSet, HashMap};
use std::iter::FromIterator;
use std::env;

mod core;
mod commands;

use self::core::config::Config;
use self::core::handler::Handler;
use self::core::params::Params;

fn main() {
    env::set_var("RUST_LOG", "INFO");
    env_logger::init().expect("Unable to init env_logger");

    let params: Params = Params::set().unwrap();
    let user_vec: Vec<UserId> = params.owners.into_iter().map(|o| UserId(o)).collect();
    let owner_set: HashSet<UserId> = HashSet::from_iter(user_vec);
    let mut config = Config{commands: HashMap::new(), prefix: "./".to_string()};
    info!("Loading Commands");
    config.init();
    
    let mut client = Client::new(&params.token, Handler{cfg: config})
        .expect("Error creating client");

    client.with_framework(StandardFramework::new()
        .configure(|c| c.owners(owner_set)));

    if let Err(why) = client.start() {
        error!("ERROR: {:?}", why);
    }
}
