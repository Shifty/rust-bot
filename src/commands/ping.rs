
use std::collections::VecDeque;
use serenity::prelude::Context;
use serenity::model::channel::Message;

pub fn ping(_ctx: Context, msg: Message, _args: VecDeque<String>) {
    let _ = msg.channel_id.say("Pong!");
}